//
//  Protocols.swift
//  Navigation Drawer Example
//
//  Created by Ashik Saeed Supto on 4/11/19.
//  Copyright © 2019 Cloudly Infotech Limited. All rights reserved.
//


protocol MenuButtonTappedDelegate{
    
    func onMenuButtonTapped()
    
}

protocol MenuItemTappedDelegate {
    func onFirstItemTapped()
    
    func onSecondItemTapped()
}
