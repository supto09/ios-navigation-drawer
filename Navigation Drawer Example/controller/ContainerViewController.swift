//
//  ContainerViewController.swift
//  Navigation Drawer Example
//
//  Created by Ashik Saeed Supto on 4/10/19.
//  Copyright © 2019 Cloudly Infotech Limited. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {

    var uiNavigationController: UINavigationController!
    var menuViewController: MenuViewController!
    var homeController : HomeViewController!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureHomeViewController()
        configureMenuViewController()
    }
    

    func configureHomeViewController(){
       
        // Instantiate the desired view controller from the storyboard using the view controllers identifier
        // Cast it as the custom view controller type you created in order to access it's properties and methods
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        uiNavigationController = storyboard.instantiateViewController(withIdentifier: "UiNavigationController") as? UINavigationController

        // get the homwViewController from UINavigationController and set the delegate value
        homeController = uiNavigationController.viewControllers[0] as? HomeViewController
        homeController.menuButtonTappedDelegate = self
        
        // add the UINavigationController as the child of ContainerViewController
        view.addSubview(uiNavigationController.view)
        addChild(uiNavigationController)
        uiNavigationController.didMove(toParent: self)
    }
    
    func configureMenuViewController(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        menuViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
        
        menuViewController.menuItemTappedDelegate = self
        
        view.insertSubview(menuViewController.view, at: 0)
        addChild(menuViewController)
        menuViewController.didMove(toParent: self)
    }
}

extension ContainerViewController : MenuButtonTappedDelegate{
    func onMenuButtonTapped() {
        let homeControllerPosition = self.uiNavigationController.view.frame.origin.x;
        
        if(homeControllerPosition == 0){
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                self.uiNavigationController.view.frame.origin.x = self.uiNavigationController.view.frame.width - 90
            }, completion: nil)
        } else{
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                self.uiNavigationController.view.frame.origin.x = 0
            }, completion: nil)
        }
    }
}


extension ContainerViewController : MenuItemTappedDelegate{
    func onFirstItemTapped() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.uiNavigationController.view.frame.origin.x = 0
        }, completion: { (true) in
           self.homeController.performSegue(withIdentifier: "detailsSague", sender: nil)
        })
    }
    
    func onSecondItemTapped() {
        
    }
}
