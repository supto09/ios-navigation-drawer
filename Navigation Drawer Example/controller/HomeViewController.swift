//
//  HomeViewController.swift
//  Navigation Drawer Example
//
//  Created by Ashik Saeed Supto on 4/10/19.
//  Copyright © 2019 Cloudly Infotech Limited. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    
    var menuButtonTappedDelegate : MenuButtonTappedDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onMenuTapped(_ sender: Any) {
        menuButtonTappedDelegate.onMenuButtonTapped()
    }
}
