//
//  MenuViewController.swift
//  Navigation Drawer Example
//
//  Created by Ashik Saeed Supto on 4/10/19.
//  Copyright © 2019 Cloudly Infotech Limited. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    
    var menuItemTappedDelegate : MenuItemTappedDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func onFirstItemTapped(_ sender: Any) {
        menuItemTappedDelegate.onFirstItemTapped()
    }
    @IBAction func onSecondItemTapped(_ sender: Any) {
        menuItemTappedDelegate.onSecondItemTapped()
    }
}
